'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class CustomerSchema extends Schema {
  up() {
    this.create('customers', (table) => {
      table.increments();
      table.string('first_name').notNullable();
      table.string('last_name').notNullable();
      table.string('phone').unique();
      table.string('email').notNullable().unique();
      table.string('password').notNullable();
      table.string('avatar');
      table.boolean('isVerified').defaultTo(0);
      table.string('type').defaultTo('customer');
      table.string('token');
      table.string('facebook_token');
      table.string('google_token');
      table.string('login_source');
      table.string('provider');
      table.string('provider_id');
      table.timestamps();
    });
  }

  down() {
    this.drop('customers');
  }
}

module.exports = CustomerSchema;
