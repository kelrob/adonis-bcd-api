'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrderSchema extends Schema {
  up () {
    this.create('orders', (table) => {
      table.increments()
      table.integer('customer_id').unsigned().notNullable()
      table.integer('product_id').unsigned().notNullable()
      table.float('agreed_price').notNullable()
      table.integer('quantity').notNullable()
      table.text('delivery_information')
      table.string('payment_status')
      table.string('order_type')
      table.integer('order_status_id').unsigned().notNullable()
      table.foreign('customer_id').references('Customers.id')
      table.foreign('product_id').references('Products.id')
      table.foreign('order_status_id').references('Order_Statuses.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('orders')
  }
}

module.exports = OrderSchema
