'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductRequestSchema extends Schema {
  up () {
    this.create('product_requests', (table) => {
      table.increments()
      table.string('title').notNullable()
      table.text('description').notNullable()
      table.string('image_url').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('product_requests')
  }
}

module.exports = ProductRequestSchema
