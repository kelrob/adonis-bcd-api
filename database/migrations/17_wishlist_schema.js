'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WishlistSchema extends Schema {
  up () {
    this.create('wishlists', (table) => {
      table.increments()
      table.integer('customer_id').unsigned().notNullable()
      table.integer('product_id').unsigned().notNullable()
      table.foreign('customer_id').references('Customers.id')
      table.foreign('product_id').references('Products.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('wishlists')
  }
}

module.exports = WishlistSchema
