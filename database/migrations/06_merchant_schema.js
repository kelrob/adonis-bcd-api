'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class MerchantSchema extends Schema {
  up() {
    this.create('merchants', (table) => {
      table.increments();
      table.string('first_name').notNullable();
      table.string('last_name').notNullable();
      table.string('email').notNullable().unique();
      table.string('password').notNullable();
      table.string('phone').unique().notNullable();
      table.string('business_name').notNullable();
      table.string('rc_number').notNullable();
      table.string('location').notNullable();
      table.string('business_address').notNullable();
      table.integer('plan_id').unsigned();
      table.boolean('isVerified').defaultTo(0);
      table.foreign('plan_id').references('Plans.id');
      table.string('type').defaultTo('merchant');
      table.string('token');
      table.timestamps();
    });
  }

  down() {
    this.drop('merchants');
  }
}

module.exports = MerchantSchema;
