'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class AdminSchema extends Schema {
  up() {
    this.create('admins', (table) => {
      table.increments();
      table.string('first_name').notNullable();
      table.string('last_name').notNullable();
      table.string('email').notNullable().unique();
      table.string('password');
      table.string('phone').unique();
      table.boolean('isVerified').defaultTo(0);
      table.string('token');
      table.string('type').defaultTo('admin');
      table.timestamps();
    });
  }

  down() {
    this.drop('admins');
  }
}

module.exports = AdminSchema;
