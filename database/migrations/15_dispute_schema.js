'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DisputeSchema extends Schema {
  up () {
    this.create('disputes', (table) => {
      table.increments()
      table.integer('customer_id').unsigned().notNullable()
      table.integer('merchant_id').unsigned().notNullable()
      table.integer('order_id').unsigned().notNullable()
      table.text('complaint').notNullable()
      table.boolean('resolved').default(0)
      table.text('resolution')
      table.text('dispute_status')
      table.foreign('customer_id').references('customers.id')
      table.foreign('merchant_id').references('Merchants.id')
      table.foreign('order_id').references('Orders.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('disputes')
  }
}

module.exports = DisputeSchema
