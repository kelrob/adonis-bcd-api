"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class ProductSchema extends Schema {
  up() {
    this.create("products", (table) => {
      table.increments();
      table.string("name").notNullable();
      table.integer("category_id").unsigned().notNullable();
      table.integer("sub_category_id").unsigned().notNullable();
      table.integer("merchant_id").unsigned().notNullable();
      table.text("specification").notNullable();
      table.float("price").notNullable();
      table.text("description").notNullable();
      table.boolean("visible").default(0);
      table.boolean("approved").default(0);
      table.foreign("category_id").references("Categories.id");
      table.foreign("sub_category_id").references("Sub_Categories.id");
      table.foreign("merchant_id").references("Merchants.id");
      table.timestamps();
    });
  }

  down() {
    this.drop("products");
  }
}

module.exports = ProductSchema;
