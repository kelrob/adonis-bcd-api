'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentSchema extends Schema {
  up () {
    this.create('payments', (table) => {
      table.increments()
      table.integer('product_id').unsigned().notNullable()
      table.integer('merchant_id').unsigned().notNullable()
      table.integer('customer_id').unsigned().notNullable()
      table.integer('order_id').unsigned().notNullable()
      table.string('transaction_id').notNullable()
      table.foreign('product_id').references('Products.id')
      table.foreign('merchant_id').references('Merchants.id')
      table.foreign('customer_id').references('Customers.id')
      table.foreign('order_id').references('Orders.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('payments')
  }
}

module.exports = PaymentSchema
