'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SubCategorySchema extends Schema {
  up () {
    this.create('sub_categories', (table) => {
      table.increments()
      table.integer('category_id').unsigned().notNullable()
      table.string('name').notNullable()
      table.string('alias').notNullable()
      table.foreign('category_id').references('Categories.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('sub_categories')
  }
}

module.exports = SubCategorySchema
