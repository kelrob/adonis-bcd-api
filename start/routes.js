'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');

Route.on('/').render('welcome');

Route.group(() => {
  // Auth
  Route.post('register/:type', 'AuthController.register');
  Route.post('login/:type', 'AuthController.login');
  Route.get('verify/:account/:email/:code', 'AuthController.verifyAccount');
  Route.post('resend-verification-code', 'AuthController.resendVerifyAccount');
  Route.post('forgot-password', 'AuthController.forgotPassword');
  Route.post('reset-password/:token', 'AuthController.resetPassword');
  Route.post('change-password', 'AuthController.changePassword');

  // Customer Social Media Auth
  Route.get('customer/login/:provider', 'AuthController.redirectToProvider');
  Route.get(
    '/customer/callback/:provider',
    'AuthController.handleProviderCallback'
  );

  // Categories
  Route.get('categories', 'CategoryController.index');
  Route.post('category/create', 'CategoryController.store');
  Route.put('category/update/:id', 'CategoryController.update');
  Route.delete('category/delete/:id', 'CategoryController.destroy');

  // Sub Categories
  Route.get('sub-categories', 'SubCategoryController.index');
  Route.post('sub-category/create', 'SubCategoryController.store');
  Route.put('sub-category/update/:id', 'SubCategoryController.update');
  Route.delete('sub-category/delete/:id', 'SubCategoryController.destroy');

  // Customers
  Route.get('customers', 'CustomerController.index');

  // Products
  Route.get('products', 'ProductController.index');
  Route.get('product/:id', 'ProductController.show');
  Route.post('product/create', 'ProductController.store');
  Route.delete('/product/delete/:id/:account', 'ProductController.destroy');

  // Orders
  Route.get('orders', 'OrderController.index');

  // OrderSatus
  Route.get('order-status/all', 'OrderStatusController.index');
  Route.post('order-status/create', 'OrderStatusController.store');
  Route.put('order-status/update/:id', 'OrderStatusController.update');
  Route.delete('order-status/delete/:id', 'OrderStatusController.destroy');
}).prefix('api/v1');
