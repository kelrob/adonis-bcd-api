'use strict'

const SubCategory = use('App/Models/SubCategory');
const Category = use('App/Models/Category');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with subcategories
 */
class SubCategoryController {
  /**
   * Show a list of all subcategories.
   * GET subcategories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    let sub_categories = await SubCategory.query().with('category').fetch()

    return response.json({ sub_categories })
  }

  /**
   * Render a form to be used for creating a new subcategory.
   * GET subcategories/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new subcategory.
   * POST subcategories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response, auth }) {
    const user =  await auth.authenticator('adminAuth').getUser();

    if (user.type === 'admin') {

      let {name, category_id} = request.all();
      let alias = name.replace(/\W+/g, '-').toLowerCase();

      if (!name || !category_id) {
        return response.json({ satus: false, message: 'SubCategory name and Category Id is required'})
      } else {
        const subCategoryExist = await SubCategory.findBy('name', name)
        const categoryExist = await Category.find(category_id)

        if (subCategoryExist ) {
          return response.json({ status: false, message: 'Sub Category Already Exist' })
        } else if (!categoryExist) {
          return response.json({ status: false, message: 'Category not Found' })
        } else {

          const subCategory = new SubCategory()
          subCategory.name = name
          subCategory.alias = alias
          subCategory.category_id = category_id

          await subCategory.save()

          return response.json(subCategory);
        }
      }
    } else {
      return response.json({ status: false, message: 'Unathorized access'})
    }
  }

  /**
   * Display a single subcategory.
   * GET subcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing subcategory.
   * GET subcategories/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update subcategory details.
   * PUT or PATCH subcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response, auth }) {
    const user =  await auth.authenticator('adminAuth').getUser();

    if (!request.body.name) {
      return response.json({ status: false, message: 'Sub Categoy name is required' })
    }

    const subCatName = request.body.name

    if (user.type === 'admin') {
      const subCatId = params.id;

      const subCategory = await SubCategory.find(subCatId)
      if (subCategory) {
        subCategory.name = subCatName;
        subCategory.alias = subCatName.replace(/\W+/g, '-').toLowerCase();

        subCategory.save()
        return response.json({ status: true, subCategory })
      } else {
        return response.json({status: false, message: 'Sub Category does not exist'})
      }

    } else {
      return response.json({
        status: false,
        message: 'Unauthorized access'
      })
    }
  }

  /**
   * Delete a subcategory with id.
   * DELETE subcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response, auth }) {
    const user =  await auth.authenticator('adminAuth').getUser();

    if (user.type === 'admin') {

      const subCatId = params.id;
      const subCategory = await SubCategory.find(subCatId)

      if (subCategory) {
        subCategory.delete()
        return response.json({ status: true, message: 'SubCategory Deleted' })
      } else {
        return response.json({status: false, message: 'SubCategory does not exist'})
      }

    } else {
      return response.json({ status: false, message: 'Unathorized access' })
    }
  }
}

module.exports = SubCategoryController
