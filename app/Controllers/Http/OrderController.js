'use strict';

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Order = use('App/Models/Order');
const Customer = use('App/Models/Customer');
const Product = use('App/Models/Product');
const OrderStatus = use('App/Models/OrderStatus');

const { validate } = use('Validator');

/**
 * Resourceful controller for interacting with orders
 */
class OrderController {
  /**
   * Show a list of all orders.
   * GET orders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    let orders = await Order.query().fetch();

    return response.json({ orders });
  }

  /**
   * Render a form to be used for creating a new order.
   * GET orders/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new order.
   * POST orders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    let validation = await validate(request.all(), {
      customer_id: 'required',
      product_id: 'required',
      agreed_price: 'required',
      quantity: 'required',
      delivery_information: 'required',
      payment_status: 'required',
      order_type: 'required',
      order_status_id: 'required',
    });

    if (validation.fails()) {
      return response.json({ status: false, messages: validation.messages() });
    }

    const customer = await Customer.find(request.body.customer_id);
    const product = await Product.find(request.body.product_id);
    const orderStatus = await OrderStatus.find(request.body.order_status_id);

    let noExist = '';
    if (product === null) {
      notExist += 'productID, ';
    }
    if (customer === null) {
      notExist += 'customerId, ';
    }
    if (orderStatus === null) {
      notExist += 'orderStatusId, ';
    }

    if (
      productExist === null ||
      customerExist === null ||
      orderStatusExist === null
    ) {
      return response.json({
        status: false,
        message: `${notExist}does not exist`,
      });
    } else {
      // Get the available goods in stock and check if it meets order quantity.
      return response.json({ status: false, product });
    }
  }

  /**
   * Display a single order.
   * GET orders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing order.
   * GET orders/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update order details.
   * PUT or PATCH orders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a order with id.
   * DELETE orders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, auth, response }) {}
}

module.exports = OrderController;
