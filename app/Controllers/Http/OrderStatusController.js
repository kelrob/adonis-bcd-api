'use strict';

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const OrderStatus = use('App/Models/OrderStatus');
const { validate } = use('Validator');

/**
 * Resourceful controller for interacting with orderstatuses
 */
class OrderStatusController {
  /**
   * Show a list of all orderstatuses.
   * GET orderstatuses
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    let orderStatus = await OrderStatus.query().fetch();

    return response.json({ orderStatus });
  }

  /**
   * Render a form to be used for creating a new orderstatus.
   * GET orderstatuses/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new orderstatus.
   * POST orderstatuses
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    const user = await auth.authenticator('adminAuth').getUser();

    let validation = await validate(request.all(), {
      status: `required|unique:order_statuses`,
      description: 'required',
    });

    if (validation.fails()) {
      return response.json({ status: false, messages: validation.messages() });
    }

    if (user.type === 'admin') {
      const orderStatus = await OrderStatus.create(request.all());

      return response.json({
        status: true,
        message: 'Order Status Created',
        orderStatus,
      });
    } else {
      return response.json({
        status: false,
        message: 'You do not have the permisssion to perform this action',
      });
    }
  }

  /**
   * Display a single orderstatus.
   * GET orderstatuses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing orderstatus.
   * GET orderstatuses/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update orderstatus details.
   * PUT or PATCH orderstatuses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, auth, response, request }) {
    const user = await auth.authenticator('adminAuth').getUser();

    if (request.body.status && request.body.description) {
      const status = request.body.status;
      const description = request.body.description;

      if (user.type === 'admin') {
        const orderStatusId = params.id;
        const orderStatus = await OrderStatus.find(orderStatusId);

        if (orderStatus) {
          orderStatus.status = status;
          orderStatus.description = description;

          orderStatus.save();
          return response.json({ status: true, orderStatus });
        } else {
          return response.json({
            status: false,
            message: 'Category does not exist',
          });
        }
      } else {
        return response.json({
          status: false,
          message: 'Unauthorized access',
        });
      }
    } else {
      return response.json({
        status: false,
        message: 'All Fields are required',
      });
    }
  }

  /**
   * Delete a orderstatus with id.
   * DELETE orderstatuses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, auth, response }) {
    const user = await auth.authenticator('adminAuth').getUser();

    if (user.type === 'admin') {
      const orderStatusId = params.id;
      const orderStatus = await OrderStatus.find(orderStatusId);

      if (orderStatus) {
        orderStatus.delete();
        return response.json({ status: true, message: 'OrderStatus Deleted' });
      } else {
        return response.json({
          status: false,
          message: 'OrderStatus does not exist',
        });
      }
    } else {
      return response.json({ status: false, message: 'Unathorized access' });
    }
  }
}

module.exports = OrderStatusController;
