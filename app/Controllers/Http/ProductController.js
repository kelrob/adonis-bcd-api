'use strict';

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Product = use('App/Models/Product');
const Category = use('App/Models/Category');
const Merchant = use('App/Models/Merchant');
const SubCategory = use('App/Models/SubCategory');

/**
 * Resourceful controller for interacting with products
 */
class ProductController {
  /**
   * Show a list of all products.
   * GET products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    let products = await Product.query()
      .with('category')
      .with('merchant')
      .with('sub_category')
      .fetch();

    return response.json({ products });
  }

  /**
   * Render a form to be used for creating a new product.
   * GET products/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new product.
   * POST products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    const user = await auth.authenticator('merchantAuth').getUser();

    if (user.type === 'merchant') {
      let {
        name,
        category_id,
        sub_category_id,
        merchant_id,
        specification,
        price,
        description,
      } = request.all();

      if (
        !name ||
        !category_id ||
        !sub_category_id ||
        !merchant_id ||
        !specification ||
        !price ||
        !description
      ) {
        return response.json({
          status: false,
          message: 'All fields are required',
        });
      }

      const catExist = await Category.find(category_id);
      const subCatExist = await SubCategory.find(sub_category_id);

      if (!catExist || !subCatExist) {
        response.json({
          status: false,
          message: 'Category or SubCategory does not exist',
        });
      } else {
        request.body.merchant_id = user.id;
        const product = Product.create(request.all());

        // Upload Image Function here

        return response.json({
          status: true,
          message: 'Product added successfully',
          product,
        });
      }
    }
  }

  /**
   * Display a single product.
   * GET products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const productId = params.id;
    const productExist = await Product.find(productId);

    if (productExist) {
      const product = await Product.query()
        .where('id', productId)
        .with('merchant')
        .with('category')
        .with('sub_category')
        .fetch();
      return response.json({ status: true, product });
    } else {
      return response.json({ status: false, message: 'Product not found' });
    }
  }

  /**
   * Render a form to update an existing product.
   * GET products/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update product details.
   * PUT or PATCH products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a product with id.
   * DELETE products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response, auth }) {
    const productId = params.id;
    const product = await Product.find(productId);
    let account = params.account;

    if (account === 'admin') {
      account = await auth.authenticator('adminAuth').getUser();
    } else if (account === 'merchant') {
      account = await auth.authenticator('merchantAuth').getUser();
    }

    if (
      product &&
      (account.id === product.merchant_id || account.type === 'admin')
    ) {
      product.delete();
      return response.json({ status: true, message: 'Product Deleted' });
    } else {
      return response.json({
        status: false,
        message:
          'Product does not exist or you do not have permission to perform action',
      });
    }
  }
}

module.exports = ProductController;
