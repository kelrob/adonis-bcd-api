'use strict';

const Util = use('App/Controllers/Http/UtilController');
const Admin = use('App/Models/Admin');
const Customer = use('App/Models/Customer');
const Merchant = use('App/Models/Merchant');
const { validate } = use('Validator');
const Hash = use('Hash');

const crypto = require('crypto');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const utils = new Util();

class AuthController {
  async register({ request, auth, response, params }) {
    let account;
    let accountType = params.type;

    let validation;
    // Validation for all User type of account
    if (accountType === 'admin' || accountType === 'customer') {
      validation = await validate(request.all(), {
        email: `required|email|unique:${accountType}s,email`,
        first_name: 'required',
        last_name: 'required',
        password: 'required',
        phone: `required|unique:${accountType}s,phone`,
      });
    } else if (accountType === 'merchant') {
      validation = await validate(request.all(), {
        email: `required|email|unique:${accountType}s,email`,
        first_name: 'required',
        last_name: 'required',
        password: 'required',
        phone: `required|unique:${accountType}s,phone`,
        business_name: 'required',
        rc_number: `required|unique:${accountType}s,rc_number`,
        location: 'required',
        business_address: 'required',
      });
    } else {
      return response.json({ status: false, message: 'Invalid account Type' });
    }

    if (validation.fails()) {
      return response.json({ status: false, messages: validation.messages() });
    }

    if (accountType === 'admin') {
      account = await Admin.create(request.all());
    } else if (accountType === 'customer') {
      account = await Customer.create(request.all());
    } else if (accountType === 'merchant') {
      account = await Merchant.create(request.all());
    }

    // Generate token for user;
    let token = await auth.generate(account);
    Object.assign(account, token);

    // Create Verification code and send mail
    const cipher = crypto.createCipher('aes-128-cbc', process.env.CRYPTO_KEY);
    let verificationCode = cipher.update(account.email, 'utf8', 'hex');
    verificationCode += cipher.final('hex');

    const html = `
			<h3>Hi ${account.first_name} ${account.last_name}</h3><br>
			<p>Please use this verification <a href="${verificationCode}">Click Me</a> to verify your account.</p><br>
			<p>If you did not request this, please ignore this email.</p>
		`;

    // Send a Welcome Mail
    // Please note that when sending mail, we need to add the account type to the email link for verification
    const msg = {
      to: account.email,
      from: 'info@bcd.ng',
      subject: 'Welcome to BCD',
      text: 'Congratulations on owning a store',
      html,
    };

    sgMail.send(msg);

    return response.json({
      status: true,
      message: 'Kindly Check Your email account to verify your account',
      account,
    });
  }

  async login({ request, auth, response, params }) {
    let account;
    let accountType = params.type;

    let validation;
    validation = await validate(request.all(), {
      email: 'required|email',
      password: 'required',
    });

    if (validation.fails()) {
      return response.json({ status: false, messages: validation.messages() });
    }

    let { email, password } = request.all();

    try {
      if (accountType === 'admin') {
        await auth.authenticator('adminAuth').attempt(email, password);
        account = await Admin.findBy('email', email);
      } else if (accountType === 'customer') {
        await auth.authenticator('customerAuth').attempt(email, password);
        account = await Customer.findBy('email', email);
      } else if (accountType === 'merchant') {
        await auth.authenticator('merchantAuth').attempt(email, password);
        account = await Merchant.findBy('email', email);
      } else {
        return response.json({
          status: false,
          message: 'Invalid account Type',
        });
      }

      if (account.isVerified === 1) {
        let token = await auth.generate(account);

        Object.assign(account, token);
        return response.json({ status: true, account });
      } else {
        return response.json({
          status: false,
          message: 'Account has not been verified',
        });
      }
    } catch (e) {
      console.log(e);
      return response.json({
        status: false,
        message: 'Invalid credentials submitted!',
      });
    }
  }

  async verifyAccount({ request, auth, response, params }) {
    const account = params.account;
    const email = params.email;
    const code = params.code;

    if (!account || !email || !code) {
      return response.json({
        status: false,
        message: 'All Parameters have not been set',
      });
    }

    // const decryptedCode = Encryption.decrypt(code);
    const decipher = crypto.createDecipher(
      'aes-128-cbc',
      process.env.CRYPTO_KEY
    );
    let decryptedCode = decipher.update(code, 'hex', 'utf8');
    decryptedCode += decipher.final('utf8');

    let user = '';
    if (account === 'admin') {
      user = await Admin.findBy('email', email);
    } else if (account === 'customer') {
      user = await Customer.findBy('email', email);
    } else if (account === 'merchant') {
      user = await Customer.findBy('email', email);
    } else {
      return response.json({ status: false, message: 'Invalid account type' });
    }

    if (user === null) {
      return response.json({ status: false, message: 'Invaid User Account' });
    } else {
      if (decryptedCode === email) {
        if (user.isVerified === 0) {
          user.isVerified = 1;
          user.save();
          return response.json({
            status: true,
            message: 'Account Verified Successfully',
            user,
          });
        } else {
          return response.json({
            status: false,
            message: 'Account has already been verified',
          });
        }
      } else {
        return response.json({ status: false, message: 'Invalid token' });
      }
    }
  }

  async resendVerifyAccount({ request, response }) {
    const email = request.body.email;
    const accountType = request.body.account;

    if (!email || !accountType) {
      return response.json({
        status: false,
        message: 'All Fields are required',
      });
    }

    let user;
    if (accountType === 'admin') {
      user = await Admin.findBy('email', email);
    } else if (accountType === 'customer') {
      user = await Customer.findBy('email', email);
    } else if (accountType === 'merchant') {
      user = await Merchant.findBy('email', email);
    } else {
      return response.json({ status: false, message: 'Invalid Account Type' });
    }

    if (user === null) {
      return response.json({
        status: false,
        message: 'Account does not exist',
      });
    } else {
      // Create Verification code and send mail
      const cipher = crypto.createCipher('aes-128-cbc', process.env.CRYPTO_KEY);
      let verificationCode = cipher.update(user.email, 'utf8', 'hex');
      verificationCode += cipher.final('hex');

      const html = `
        <h3>Hi ${user.first_name} ${user.last_name}</h3><br>
        <p>Please use this verification <a href="${verificationCode}">Click Me</a> to verify your account.</p><br>
        <p>If you did not request this, please ignore this email.</p>
      `;

      // Send a Welcome Mail
      // Please note that when sending mail, we need to add the account type to the email link for verification
      const msg = {
        to: user.email,
        from: 'info@bcd.ng',
        subject: 'Welcome to BCD',
        text: 'Congratulations on owning a store',
        html,
      };

      sgMail.send(msg);

      return response.json({
        status: true,
        message: 'Verification Link sent to your email',
      });
    }
  }

  async forgotPassword({ request, response }) {
    const email = request.body.email;
    const accountType = request.body.account;

    if (!email || !accountType) {
      return response.json({
        status: false,
        message: 'All Parameters have not been set',
      });
    }

    let user;
    if (accountType === 'admin') {
      user = await Admin.findBy('email', email);
    } else if (accountType === 'customer') {
      user = await Customer.findBy('email', email);
    } else if (accountType === 'merchant') {
      user = await Merchant.findBy('email', email);
    } else {
      return response.json({ status: false, message: 'Invalid Account Type' });
    }

    if (user !== null) {
      user.token = utils.generateUniqueKey(50);
      user.save();

      // Send a ForgotPassword Mail
      // Please note that when sending mail, we need to add the account type to the email link for verification

      const html = `
        <h3>Hi ${user.first_name} ${user.last_name}</h3><br>
        <p>We have received a request to change your reset your password, here is your token: ${user.token}</p>
      `;

      const msg = {
        to: user.email,
        from: 'info@bcd.ng',
        subject: 'Forgot Password',
        text: 'Password Reset',
        html,
      };

      sgMail.send(msg);

      return response.json({
        status: true,
        message: 'Please check your email for instructions',
      });
    } else {
      return response.json({
        status: false,
        message: 'Account does not exist',
      });
    }
  }

  async resetPassword({ response, params, request }) {
    const email = request.body.email;
    const accountType = request.body.account;
    const password = request.body.password;
    const passwordAgain = request.body.password_again;
    const token = params.token;

    if (!email || accountType || !password || !passwordAgain) {
      return (
        response, json({ status: false, message: 'All Fields are required' })
      );
    }

    if (!token) {
      return (
        response,
        json({
          status: false,
          message: 'Token needs to be passed to the parameter',
        })
      );
    }

    let user;
    if (accountType === 'admin') {
      user = await Admin.findBy('email', email);
    } else if (accountType === 'customer') {
      user = await Customer.findBy('email', email);
    } else if (accountType === 'merchant') {
      user = await Merchant.findBy('email', email);
    } else {
      return response.json({ status: false, message: 'Invalid Account Type' });
    }

    if (user !== null) {
      if (user.token === token) {
        if (password === passwordAgain) {
          user.password = password;
          user.token = utils.generateUniqueKey(50);
          user.save();

          return response.json({
            status: true,
            message: 'Password Reset successfuly',
          });
        } else {
          return response.json({
            status: true,
            message: 'Passwords do not match',
          });
        }
      } else {
        return response.json({ status: false, message: 'Invalid Key Token' });
      }
    } else {
      return response.json({
        status: false,
        message: 'User Account not found',
      });
    }
  }

  async changePassword({ request, response, auth }) {
    const currentPassword = request.body.current_password;
    const newPassword = request.body.new_password;
    const newPasswordAgain = request.body.new_password_again;
    const accountType = request.body.account;

    let validation;
    validation = await validate(request.all(), {
      current_password: 'required',
      new_password: 'required',
      new_password_again: 'required',
    });

    if (validation.fails()) {
      return response.json({ status: false, messages: validation.messages() });
    }

    let user;
    if (accountType === 'admin') {
      user = await auth.authenticator('adminAuth').getUser();
    } else if (accountType === 'customer') {
      user = await auth.authenticator('customerAuth').getUser();
    } else if (accountType === 'merchant') {
      user = await auth.authenticator('merchantAuth').getUser();
    } else {
      return response.json({ status: false, message: 'Invalid account type' });
    }

    if (newPassword === newPasswordAgain) {
      const hashedPassword = user.password;
      const passwordMatch = await Hash.verify(currentPassword, hashedPassword);

      if (passwordMatch) {
        user.password = newPassword;
        user.save();

        return response.json({
          status: false,
          message: 'Password Changed Successfully',
        });
      } else {
        return response.json({
          status: false,
          message: 'Current password entered is wrong',
        });
      }
    } else {
      return response.json({
        status: false,
        message: 'New Passowrd and New Password Again do not match',
      });
    }
  }

  async redirectToProvider({ ally, params, response }) {
    const provider = params.provider;
    if (provider === 'google' || provider === 'facebook') {
      await ally.driver(provider).redirect();
    } else {
      return response.json({
        status: false,
        message: 'Invalid Provider Supplied',
      });
    }
  }

  async handleProviderCallback({ params, ally, auth, response }) {
    // Get the provider details
    const provider = params.provider;

    try {
      // Store the user data gotten from provider
      const customerData = await ally.driver(provider).getUser();

      // Try to see if the email exist
      const authCustomer = await Customer.findBy(
        'email',
        customerData.getEmail()
      );

      if (!(authCustomer === null)) {
        let token = await auth
          .authenticator('customerAuth')
          .generate(authCustomer);

        Object.assign(authCustomer, token);
        return response.json({ status: true, customer: authCustomer });
      }

      let fullName = customerData.getName();
      let firstName = fullName.split(' ').slice(0, -1).join(' ');
      let lastName = fullName.split(' ').slice(-1).join(' ');

      const customer = new Customer();
      customer.first_name = firstName;
      customer.last_name = lastName;
      customer.email = customerData.getEmail();
      customer.avatar = customerData.getAvatar();
      customer.provider = provider;
      customer.isVerified = 1;
      customer.facebook_token =
        provider === 'facebook' ? customerData.getAccessToken() : '';
      customer.google_token =
        provider === 'google' ? customerData.getAccessToken() : '';

      await customer.save();
      let token = await auth
        .authenticator('customerAuth')
        .generate(authCustomer);

      Object.assign(authCustomer, token);
      return response.json({ status: true, customer: authCustomer });
    } catch (e) {
      console.log(e);
      return response.json({
        status: false,
        message: 'There was an error Authenticating account',
      });
    }
  }
}

module.exports = AuthController;
