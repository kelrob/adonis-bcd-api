'use strict';

const Category = use('App/Models/Category');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with categories
 */
class CategoryController {
  /**
   * Show a list of all categories.
   * GET categories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    let categories = await Category.query().with('sub_category').fetch();

    return response.json({ categories });
  }

  /**
   * Render a form to be used for creating a new category.
   * GET categories/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new category.
   * POST categories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    const user = await auth.authenticator('adminAuth').getUser();

    if (user.type === 'admin') {
      let { name } = request.all();
      let alias = name.replace(/\W+/g, '-').toLowerCase();

      if (!name || !alias) {
        return response.json({
          satus: false,
          message: 'Category name is required',
        });
      } else {
        const categoryExist = await Category.findBy('name', name);

        if (categoryExist) {
          return response.json({
            status: false,
            message: 'Category Already Exist',
          });
        } else {
          const category = new Category();
          category.name = name;
          category.alias = alias;

          await category.save();

          return response.json(category);
        }
      }
    } else {
      return response.json({ status: false, message: 'Unathorized access' });
    }
  }

  /**
   * Display a single category.
   * GET categories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing category.
   * GET categories/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update category details.
   * PUT or PATCH categories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response, auth }) {
    const user = await auth.authenticator('adminAuth').getUser();

    if (!request.body.name) {
      return response.json({
        status: false,
        message: 'Category name is required',
      });
    }

    const catName = request.body.name;

    if (user.type === 'admin') {
      const catId = params.id;

      const category = await Category.find(catId);
      if (category) {
        category.name = catName;
        category.alias = catName.replace(/\W+/g, '-').toLowerCase();

        category.save();
        return response.json({ status: true, category });
      } else {
        return response.json({
          status: false,
          message: 'Category does not exist',
        });
      }
    } else {
      return response.json({
        status: false,
        message: 'Unauthorized access',
      });
    }
  }

  /**
   * Delete a category with id.
   * DELETE categories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, response, auth }) {
    const user = await auth.authenticator('adminAuth').getUser();

    if (user.type === 'admin') {
      const catId = params.id;
      const category = await Category.find(catId);

      if (category) {
        category.delete();
        return response.json({ status: true, message: 'Category Deleted' });
      } else {
        return response.json({
          status: false,
          message: 'Category does not exist',
        });
      }
    } else {
      return response.json({ status: false, message: 'Unathorized access' });
    }
  }
}

module.exports = CategoryController;
