'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {

  merchant() {
    return this.belongsTo('App/Models/Merchant')
  }

  category() {
    return this.belongsTo('App/Models/Category')
  }

  sub_category() {
    return this.belongsTo('App/Models/SubCategory')
  }
}

module.exports = Product
