'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Category extends Model {

  sub_category() {
    return this.hasMany('App/Models/SubCategory')
  }

  product() {
    return this.hasMany('App/Models/Product')
  }
}

module.exports = Category
